# Zastosowanie Programowania Komponentoweg - Todolist_COM
## Table of contents
* [General info](#general-info)
* [Authors](#authors)

## General info

Final project for Application Component Design. The goal of the project is useful application in COM architecture to handling simple to-do list.


## Authors

**Michał Staruch**
**Daniel Wierzbicki**
**Tomasz Grymbowski**


