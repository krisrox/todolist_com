﻿namespace Todolist_COM
{
    public class Todo
    {
        public string Information { get; set; }
        public bool IsDone { get; set; }
    }
}
