﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Todolist_COM
{
    public partial class Todolist_COM : UserControl
    {
        public static List<Todo> TodoList = new List<Todo> { };
        public static TableLayoutPanel TodoListTable;
        private static IEnumerable<Todo> _sortList = TodoList;
        private static string SortChoice { get; set; }
        private static Label TodoCounter { get; set; }
        

        public static Label Header { get; set; }
        public static TableLayoutPanel MainPanel { get; set; }
        public static Panel TaskCounterPanel { get; set; }
        public static Panel SortButtonsPanel { get; set; }
        public static Panel TodoPanel { get; set; }
        public static Label TodoText { get; set; }
        public static CheckBox TodoCheckBox { get; set; }
        public static Color BackGroundColorDefault { get; set; } = Color.Beige;
        public static Button AllButton { get; set; }
        public static Button ActiveButton { get; set; }
        public static Button CompletedButton { get; set; }
        public static Button ClearCompletedButton { get; set; }
        public static Button AddCategory { get; set; }

        public static Snow[] Snowflakes;

        public static int CategoryObjectCounter = 0;

        public Todolist_COM()
        {
            InitializeComponent();
            CreateView();
        }

        public void CreateView()
        {
            MainPanel = new TableLayoutPanel
            {
                BackColor = Color.Beige,
                RowCount = 2,
                Dock = DockStyle.Fill,
            };
            MainPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 70));
            MainPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            MinimumSize = new Size(500, 500);
            Controls.Add(MainPanel);

            MainPanel.Controls.Add(Header = new Label
            {
                Text = "Todolist",
                TextAlign = ContentAlignment.TopCenter,
                Font = new Font("consolas", 50),
                ForeColor = Color.CornflowerBlue,
                Dock = DockStyle.Fill
            });

            TaskCounterPanel = new Panel
            {
                BackColor = Color.Beige,
                Width = 350,
                Height = 20,
                Anchor = AnchorStyles.Top
            };
            MainPanel.Controls.Add(TaskCounterPanel);

            TodoCounter = new Label
            {
                Text = "0",
                Location = new Point(145, 0),
                Visible = false
            };
            MainPanel.Controls.Add(TaskCounterPanel);
            TaskCounterPanel.Controls.Add(TodoCounter);

            SortButtonsPanel = new Panel
            {
                BackColor = Color.Beige,
                ForeColor = Color.Black,
                Width = 350,
                Height = 20,
                Anchor = AnchorStyles.Top
            };
            MainPanel.Controls.Add(SortButtonsPanel);

            AllButton = new Button
            {
                Text = "All",
                Location = new Point(25, 0),
                BackColor = Color.Beige,
                Width = 40,
                Height = 20
            };
            SortButtonsPanel.Controls.Add(AllButton);

            ActiveButton = new Button
            {
                Text = "Active",
                Location = new Point(70, 0),
                BackColor = Color.Beige,
                Width = 50,
                Height = 20
            };
            ActiveButton.Click += SortButtonClickEventHandler;
            SortButtonsPanel.Controls.Add(ActiveButton);

            CompletedButton = new Button
            {
                Text = "Completed",
                Location = new Point(125, 0),
                BackColor = Color.Beige,
                Width = 70,
                Height = 20
            };
            SortButtonsPanel.Controls.Add(CompletedButton);
            CompletedButton.Click += SortButtonClickEventHandler;

            AddCategory = new Button
            {
                Text = "Add category",
                Location = new Point(200, 0),
                BackColor = Color.Beige,
                Width = 80,
                Height = 20,
                Enabled = true
            };
            SortButtonsPanel.Controls.Add(AddCategory);
            AddCategory.Click += AddCategoryEventHandler;

            ClearCompletedButton = new Button
            {
                Text = "Clear",
                Location = new Point(285, 0),
                BackColor = Color.Beige,
                Width = 50,
                Height = 20,
                ForeColor = Color.Red,
                Visible = true,
                Enabled = false
            };
            SortButtonsPanel.Controls.Add(ClearCompletedButton);
            ClearCompletedButton.Click += ClearCompletedEventHandler;
            AllButton.Click += SortButtonClickEventHandler;

            var todoInput = new TextBox
            {
                Font = new Font("consolas", 20),
                ForeColor = Color.CornflowerBlue,
                BackColor = Color.Lavender,
                BorderStyle = BorderStyle.Fixed3D,
                Width = 350,
                Height = 50,
                Dock = DockStyle.Fill,
                Anchor = AnchorStyles.Top,
                Text = "Add task..."
            };

            MainPanel.Controls.Add(todoInput);
            MainPanel.AutoScroll = true;
            todoInput.KeyDown += TodoInputEventHandler;
            todoInput.GotFocus += TextGotFocus;
            todoInput.LostFocus += TextLostFocus;

            TodoListTable = new TableLayoutPanel
            {
                RowCount = 5,
                Dock = DockStyle.Fill,
                BackColor = Color.Beige,
                Width = 350,
                Anchor = AnchorStyles.Top,
                AutoSize = true
            };
            MainPanel.Focus();
            AllButton.Select();
            todoInput.Select();
            MainPanel.Controls.Add(TodoListTable);
            TodoListTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
        }

        private static void CreateTodoListDisplay()
        {
            TodoListTable.Controls.Clear();
            foreach (var todo in _sortList)
            {
                TodoPanel = new Panel
                {
                    Width = 350,
                    Height = 30,
                    Tag = TodoList.IndexOf(todo)
                };
                TodoCheckBox = new CheckBox
                {
                    Width = 20,
                    Height = 30,
                    Location = new Point(0, 0),
                    Anchor = AnchorStyles.Top,
                    Tag = TodoList.IndexOf(todo)
                };
                TodoText = new Label
                {
                    Text = todo.Information,
                    TextAlign = ContentAlignment.MiddleLeft,
                    Location = new Point(20, 0),
                    ForeColor = Color.CornflowerBlue,
                    Font = new Font("consolas", 20),
                    Width = 300,
                    Height = 30,
                    Anchor = AnchorStyles.Left,

                    Tag = TodoList.IndexOf(todo)
                };
                var removeButton = new Button
                {
                    Text = "X",
                    Name = "RemoveButton " + TodoList.IndexOf(todo),
                    TextAlign = ContentAlignment.TopCenter,
                    Font = new Font("consolas", 14),
                    FlatStyle = FlatStyle.Flat,
                    Width = 20,
                    Height = 30,
                    Location = new Point(330, 0),
                    Tag = TodoList.IndexOf(todo)
                };

                TodoIsDone(todo, TodoCheckBox);
                TodoStrikeOut(todo, TodoText);
                ActivateClearTaskButton(TodoCheckBox);
                UpdateTodoCounter();

                TodoListTable.Controls.Add(TodoPanel);
                TodoPanel.Controls.Add(TodoCheckBox);
                TodoPanel.Controls.Add(TodoText);
                TodoPanel.Controls.Add(removeButton);
                TodoCheckBox.Click += ClickedCheckBoxEventHandler;
                removeButton.FlatAppearance.BorderSize = 0;
                removeButton.Click += RemoveButtonEventHandler;
                TodoListTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            }
        }

        private void CreateCategoryView(string categoryName)
        {
            Point defaultLocation = new Point(10, 25);
            Color defaultColor = Color.Red;

            switch (CategoryObjectCounter)
            {
                case 1:
                    defaultLocation = new Point(94, 25);
                    defaultColor = Color.Coral;
                    break;
                case 2:
                    defaultLocation = new Point(178, 25);
                    defaultColor = Color.Maroon;
                    break;
                case 3:
                    defaultLocation = new Point(262, 25);
                    defaultColor = Color.DarkGreen;
                    AddCategory.Enabled = false;
                    break;
            }

            Button newCategory = new Button
            {
                Text = categoryName,
                Location = defaultLocation,
                Height = 20,
                Width = 80,
                ForeColor = defaultColor,
                Visible = true
            };

            SortButtonsPanel.Controls.Add(newCategory);
            CategoryObjectCounter += 1;
            TodoListTable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
        }


        private static void TodoInputEventHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                var textBox = (TextBox)sender;
                if (textBox.Text != "")
                {
                    TodoListTable.Controls.Clear();
                    TodoList.Add(new Todo
                    {
                        Information = textBox.Text
                    });
                }
                textBox.Clear();
                CreateTodoListDisplay();
            }
        }

        private static void RemoveButtonEventHandler(object sender, EventArgs e)
        {
            var info = (Button)sender;
            var todoIndex = (int)info.Tag;
            TodoList.RemoveAt(todoIndex);
            CreateTodoListDisplay();
        }

        private static void ClickedCheckBoxEventHandler(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            var index = (int)checkBox.Tag;
            TodoList[index].IsDone = !TodoList[index].IsDone;
            CreateTodoListDisplay();
        }

        private void AddCategoryEventHandler(object sender, EventArgs e)
        {
            var info = (Button)sender;
            if (info != null)
            {
                string promptValue = Prompt.ShowDialog("Enter name: ", "Category name window", BackGroundColorDefault);
                SortButtonsPanel.Height = 50;
                CreateCategoryView(promptValue);
            }
        }

        private static bool TodoIsDone(Todo todo, CheckBox checkBox)
        {
            if (todo.IsDone)
            {
                return checkBox.Checked = true;
            }
            else
            {
                return checkBox.Checked = false;
            }
        }
        private static void TodoStrikeOut(Todo todo, Label label)
        {
            if (todo.IsDone)
            {
                label.Font = new Font("consolas", 20, FontStyle.Strikeout);
                label.ForeColor = Color.Gray;
            }
        }
        private static void UpdateTodoCounter()
        {
            TodoCounter.Visible = true;
            string singularOrPlural = "";
            int count = TodoList.Count(t => t.IsDone == false);
            if (count == 1)
            {
                singularOrPlural = " item left";
            }
            else if (count > 1)
            {
                singularOrPlural = " items left";
            }
            else
            {
                TodoCounter.Visible = false;
                ClearCompletedButton.Enabled = false; //TODO
            }
            TodoCounter.Text = count + singularOrPlural;
        }
        private static void SortButtonClickEventHandler(object sender, EventArgs e)
        {
            var button = (Button)sender;
            SortChoice = button.Text;
            switch (SortChoice)
            {
                case "All":
                    _sortList = TodoList;
                    break;
                case "Completed":
                    _sortList = TodoList.Where(t => t.IsDone == true);
                    break;
                case "Active":
                    _sortList = TodoList.Where(t => t.IsDone == false);
                    break;
            }
            CreateTodoListDisplay();
        }
        private static void ClearCompletedEventHandler(object sender, EventArgs e)
        {
            TodoList.RemoveAll(t => t.IsDone == true);
            CreateTodoListDisplay();
        }

        private static void TextGotFocus(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text == "Add task...")
            {
                textBox.Text = "";
            }
        }

        private static void TextLostFocus(object sender, EventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text == "")
            {
                textBox.Text = "Add task...";
            }
        }

        private static void ActivateClearTaskButton(CheckBox checkBox)
        {
            ClearCompletedButton.Enabled = true;
            ClearCompletedButton.Enabled = checkBox.Checked;
        }

        public static string ShowPromptChangingHeader()
        {
            var promptValue = Prompt.ShowDialog("Enter new header: ", "Change header window", BackGroundColorDefault);
            return promptValue;
        } 
    }
}
