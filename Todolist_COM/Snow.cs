﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Todolist_COM
{
    public class Snow : PictureBox
    {
        public Snow()
        {
            Create();
            Move();
        }

        private readonly Random _r = new Random();

        private void Create()
        {
            Location = new Point(_r.Next(-Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Width), _r.Next(-Screen.PrimaryScreen.Bounds.Height, Screen.PrimaryScreen.Bounds.Height));
            MinimumSize = new Size(7, 7);
            Size = new Size(20, 20);
            BackgroundImage = Image.FromFile("ZPK.jpg");
        }

        private new void Move()
        {
            Timer t = new Timer { Interval = 4 };
            t.Tick += new EventHandler(t_Tick);
            t.Start();
        }

        private void t_Tick(object sender, EventArgs e)
        {
            Location += new Size(1, 3);
            if (Location.X > Screen.PrimaryScreen.Bounds.Width || Location.Y > Screen.PrimaryScreen.Bounds.Height)
                Location = new Point(_r.Next(-Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Width), 
                    _r.Next(-Screen.PrimaryScreen.Bounds.Height, Screen.PrimaryScreen.Bounds.Height));
        }
    }
}

