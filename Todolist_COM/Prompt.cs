﻿using System.Drawing;
using System.Windows.Forms;

namespace Todolist_COM
{
    public static class Prompt
    {
        public static string ShowDialog(string text, string caption, Color backGroundColor)
        {
            Form prompt = new Form()
            {
                BackColor = backGroundColor,
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label()
            {
                Left = 50, 
                Top = 20, 
                Text = text
            };
            TextBox textBox = new TextBox()
            {
                Font = new Font("consolas", 8),
                ForeColor = Color.CornflowerBlue,
                BackColor = Color.Lavender,
                Left = 50, 
                Top = 50, 
                Width = 400
            };
            Button confirmation = new Button()
            {
                Text = "Ok", 
                Left = 350, 
                Width = 100, 
                Top = 75, 
                DialogResult = DialogResult.OK
            };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }
}
