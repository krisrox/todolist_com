﻿using System;
using System.Windows.Forms;

namespace Todolist_TEST
{
    using System.Drawing;
    using Todolist_COM;

    public partial class Form1 : Form
    {
        public Color BackGroundColor { get; set; }
        public Form1()
        {
            InitializeComponent();
        }

        private int _i;
        private Timer _t;

        private void Form1_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
            BackColor = Color.Black;
            TransparencyKey = Color.Black;
            Anchor = System.Windows.Forms.AnchorStyles.Top;
            BringToFront();

            Todolist_COM.Snowflakes = new Snow[400];
            _t = new Timer();
        }

        private void t_Tick(object sender, EventArgs e)
        {
            if (_i >= 40)
            {
                _t.Stop();
                return;
            }
            Todolist_COM.Snowflakes[_i] = new Snow();
            Controls.Add(Todolist_COM.Snowflakes[_i]);
            _i++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            Todolist_COM.Header.ForeColor = colorDialog1.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            BackGroundColor = colorDialog1.Color;

            Todolist_COM.MainPanel.BackColor = BackGroundColor;
            Todolist_COM.TaskCounterPanel.BackColor = BackGroundColor;
            Todolist_COM.SortButtonsPanel.BackColor = BackGroundColor;
            Todolist_COM.TodoListTable.BackColor = BackGroundColor;
            Todolist_COM.BackGroundColorDefault = BackGroundColor;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            Todolist_COM.AllButton.BackColor = colorDialog1.Color;
            Todolist_COM.ActiveButton.BackColor = colorDialog1.Color;
            Todolist_COM.CompletedButton.BackColor = colorDialog1.Color;
            Todolist_COM.AddCategory.BackColor = colorDialog1.Color;
            Todolist_COM.ClearCompletedButton.BackColor = colorDialog1.Color;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var newHeader = Todolist_COM.ShowPromptChangingHeader();
            Todolist_COM.Header.Text = newHeader;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _t.Interval = 10;
            _t.Tick += t_Tick;
            _t.Start();
        }
    }
}
